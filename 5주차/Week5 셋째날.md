click -> winner를 바꿈 -> 리렌더링 -> 에러?



useEffect는 언제 실행되는가?





**Css-tricks 'Run useEffect only once'**

쉽게 그냥 빈 배열을 `useEffect`의 두번째 parameter로 넘겨라.

dependency가 하나도 없는 상태가 되기 때문에 - 처음 마운트 될 때에만 `useEffect`가 실행될 것



> If you use this optimization, make sure the array includes all values from the component scope that change over time and that are used by the effect. 
>
> Otherwise, your code will reference stale values from previous renders.



https://dev.to/spukas/4-ways-to-useeffect-pf6 에서는..

> On the last stage of the render cycle, `useEffect()` is called with the state, handlers and effects of that call.  
>
> So every render will have their specific properties, which will never change but React always will apply the last render result.

**When and How to use it**

It slightly differs from the class component life cycle methods. 

The main difference is that lifecycle methods always have the reference to the latest state, while `useEffect()` will cash the state, handlers and effects of each render, and it will be different from the next one.

But the good thing is that you can manipulate when to call the function inside `useEffect()` by specifying a dependency list or none.



4 possible ways to call the method :

- When Components Mounts

To run the function once, add an empty dependency list.

- On Every Component Render

Skip adding the dependency list.

- On Every Compoenent Render with a Condition

specify the list of dependencies.

- When Component Unmounts



'smoke tests'

