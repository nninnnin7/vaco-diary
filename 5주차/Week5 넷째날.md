천천히, 빠르게..



켄님이 공유해주신 글

[**'언제 constant 변수의 이름을 대문자로 명명하는가?'**](https://www.freecodecamp.org/news/when-to-capitalize-your-javascript-constants-4fabc0a4a4c4/)

이미 `const` 라는 식별자를 사용하여 변수를 선언하였기 때문에 이것이 `constant`, 즉 바뀌지 않는 변수임은 알 수 있지 않은가? 왜 굳이 이를 대문자로 명명하며, 그러한 경우는 언제인가?

> By definition, `const` creates a constant that is a read-only reference to a value. This does not mean the value it holds is immutable. It only says that the variable identifier cannot be reassigned.

해석하자면, `const` 로 생성된 변수는 해당 변수에 재할당을 금지한다는 의미이지만, **실제적으로 해당 변수에 할당된 값이 `immutable` 하다는 것을 의미하지는 않는다** 라는 말이다!





알고챌린지 문제 다시 풀어보기 (이분탐색)



개발자의 경쟁력은 **탐구력, 학습력**.

하루에 '하나' 물고 늘어져서 학습하고 해결하는거

끈질긴거

하나라도 제대로





'wrapper' 를 만든다.

이를 통해 **props조회, DOM조회, state조회** 등을 할 수 있다.



```javascript
const wrapper = mount(<Profile username="justin" name="donggyu">);

expect(wrapper).toMatchSnapshot();
```







**JEST API**

global 의 것들..

`describe(name, fn)` creates a block that groups together several related tests.

연관된 테스트들을 하나로 묶는다.

`describe.each(table)(name, fn, timeout)` 

`describe.only(name, fn)` -> `fdescribe(name, fn)` 로도 쓸 수 있다.

You can use `describe.only` if you want to run only one describe block - (이하의 테스트들을 스킵하고 해당 described test 만 진행시킬 수 있다)

`describie.only.each(table)(name, fn)` 는 유일한 복수의 테스트를 진행한다.

`describe.skip(name, fn)` 도 있다. 얘만 스킵시킬 수 있다.



이번에는 expect에 관련된 API.

`expect().toBe(value)`



`expect().toEqual(value)`

Use `.toEqual` to compare recursively all properties of object instances.

It calls `Object.is` to compare primitive values, which is even better for testing than `===` strict equality operator.



`.toEqual` and `.toBe` behave differently in this test suite, so all the tests pass - 

```javascript
const can1 = {
  flavor: 'grapefruit',
  ounces: 12,
};
const can2 = {
  flavor: 'grapefruit',
  ounces: 12,
};

describe('the La Croix cans on my desk', () => {
  test('have all the same properties', () => {
    expect(can1).toEqual(can2); // 생김새가 같은지 (equal)
  });
  test('are not the exact same can', () => {
    expect(can1).not.toBe(can2); // 바로 그 녀석인지 (be)
  });
});
```







자. 그럼 이제

Loading component에서 테스트하고 싶은 것에는 무엇이 있는가?

- speed 값대로 주어진 시간마다 리렌더링을 시행하는가?
  - setInterval의 콜백을 mocking 해서 해당 함수가 호출 되었는지 테스트하면 될 것 같다. (정확한 호출 횟수는 알 수 없으므로 `expect().toHaveBeenCalled()` 를 사용하면 되지 않을까..)
- 리렌더링을 시행했을 때 의도했던 대로 state가 변화하고 있는가?





**Enzyme API Reference**

https://enzymejs.github.io/enzyme/docs/api/

shallow Rendering - `shallow`

Full Rendering - `mount`

Static rendering - `render`

enzyme이 제공하는 각각의 컴포넌트 렌더링 방식에 의해 만들어진 wrapper가 가지고 있는 API reference.







`.contains(nodeOrNodes)`

DOM 의 존재 여부를 확인할 수 있는 메서드다. ([링크](https://enzymejs.github.io/enzyme/docs/api/ShallowWrapper/contains.html))

Returns whether or not all given react elements in the render tree. It will determine if an element in the wrapper matches the expected element by checking if the expected element has the same props as the wrapper's element and share the same values.



`.instance()`

Returns the single-node wrapper's node's underlying class instance; `this` in its methods.

함수형 컴포넌트 에서는 instance를 반환하지 않는다. (instance를 쓸 필요가 없는듯)



> 함수형 컴포넌트에서는 클래스형 컴포넌트와 달리 인스턴스 메서드 및 상태를 조회할 방법이 없습니다. 추가적으로, Hooks를 사용하는 경우 꼭 `shallow` 가 아닌 `mount` 를 사용해야 합니다. 그 이유는 `useEffect` Hook은 `shallow` 에서 작동하지 않고, 버튼 엘리먼트에 연결되어 있는 함수가 이전 함수를 가리키고 있기 때문에 ..
>
> https://velog.io/@velopert/react-testing-with-enzyme





**'소명의식'**

vocation

...







배틀을 누른다 - 로딩이 렌더링 된다 - 로딩이 사라진다

이건 문제가 아니고



로딩 컴포넌트 자체를 테스트하려면?



`jest.spyOn(object, methodName)`

Creates a mock function similar to `jest.fn` but also tracks calls to object[methodName]. Returns a Jest mock function.





### Mock Functions

There are two ways to mock functions :

1. Either by creating a mock function to use in test code,
2. or writing a manual mock to override a module dependency





