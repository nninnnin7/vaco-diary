할 일

1. 알고푸니 문제풀이
   - 풀면서 꼭 주석을 적어놓아야겠다 (나중에 봤을때 빨리 설명할 수 있게)
2. **내일 모임 어떻게 할지..**
3. 알고챌린지 다시 짜보기 - 으억 해야겠다.
4. 코드리뷰 읽어보기 - 리팩토링





**Backtracking**

> Backtracking is a general algorithm for finding all solutions to some computational problems, notably constraint satisfaction problems, that **incrementally builds candidates** to the solutions, **and abandons a candidate as soon as it determines that the candidate cannot possibly be completed to a valid solution**.



리트코드 1110번 `Delete Nodes And Return Forest` 에 백트래킹을 적용해서 생각해보면..

노드의 자식노드들(left, right)에 traverse와 삭제할 노드인지를 확인하는 함수를 적용시켜 재귀호출 할 때 상위노드가 삭제되어야 하는 노드인지 아닌지를 판별하는 boolean값을 전달하여 해당 노드가 새로운 root로 결과에 추가되어야 하는지 아닌지를 판별하고 손절(?) 할 수 있다. 이 부분을 백트래킹이라 볼 수 있지 않을지..

명확하지가 않다.



Geeks for geeks에 의하면..

There are three types of problems in backtracking.

1.  Decision Problem - In this, we search for a feasible solution.
2. Optimization Problem - In this, we search for the best solution.
3. Enumeration Problem - In this, we find all feasible solutions.

아마 내가 푼 문제는 3번에 속하지 않을지 (모든 가능한 솔루션을 찾아내는 것)

여기에서는 '가능한 솔루션' 에 포함되는 것이 result로 추가될 수 있는 노드들, 일까?



조금 더 보자.



How to determin if a problem can be solve using Backtracking?

Generally, every const