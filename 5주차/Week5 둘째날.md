**Side effect**

(클래스 기반 컴포넌트와는 달리) 함수형 컴포넌트를 사용하며 고려해야 할 것.



리액트 (함수형) 컴포넌트의 원래 기능은 UI를 구현하는 것. 그러나 이를 위한 데이터 등을 가져오는 과정에서 fetching 등 원래는 의도하지 않았던 side effect가 있을 수 있다.



컴포넌트 외부 정보 **'구독'** 하기

모든 외부정보를 state를 이용한다면 실시간으로 정보를 '구독' 해서 UI를 변경하기 어렵다?





**무엇이 '외부요인' 인가?**

일단 UI를 변경시키는 것은 외부요인이 아니다.

데이터를 fetching하는 것은 외부요인이다. (side effect)



**그런데 꼭 useEffect를 사용해야 하는 이유가 있을까?**

state의 변경은 외부정보는 아니다.



버튼눌러서 한번 렌더링 / 렌더링 될 때 마다 뿌려주기





\- 승패 여부

  \- 프로필 사진 : avatar_url

  \- 점수 : 

  \- Github Username : login

  \- 이름 : name

  \- 지역 : location

  \- Followers Count : followers

  \- Following Count : following

  \- Repository Count : public_repos







useEffect



Accepts a function that contains **imperative, possibly effectful** code.



**Mutations, subscriptions, timers, logging**, and other side effects are not allowed inside the main body of a function component. (referred to as React's render phase).



Doing so will lead to confusing bugs and inconsistencies in the UI.



Instead, use `useEffect`. The function passed to `useEffect` will run after the render is committed to the screen. Think of effects as an escape hatch from React's purely functional world into the imperative world. 





