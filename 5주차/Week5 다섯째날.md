CSS reflow

What is DOM reflow? [Stack Overflow](https://stackoverflow.com/questions/27637184/what-is-dom-reflow)



A reflow computes the **layout** of the page.

A reflow on an element recomputes the dimensions and position of the element, and it also triggers further reflows on that element's children, ancestors and elements that appear after it in the DOM.

Then it calls a final repaint.

Reflowing is very expensive, but unforunately it can be triggered easily.







// 뭘 어떻게 테스트해야 하는가?

로딩 컴포넌트는 어떻게 작동하는가?

깜빡, 깜빡, 깜빡.

`speed` 라는 초마다, `setInterval` 에 전달된 콜백함수를 실행하여 `text` 라는 스테이트를 바꾸고 리렌더링한다.



이것이 정상적으로 작동하는지 확인하려면?

- setInterval에 전달된 콜백함수가 실행될 때 마다, 내가 원하는 대로 text가 변경되었는지 (스테이트, 혹은 렌더링 된 결과) 확인한다



사용할 것들

1. [spyOn](https://www.daleseo.com/jest-fn-spy-on/)
2. [Timer Mocks](https://jestjs.io/docs/en/timer-mocks)



**useEffect 는 언제 실행되는가?**

useEffect는 render sequence의 맨 끝에 실행된다.

**useEffect의 콜백함수 내에서 setState를 여러개 실행하면 순서가 보장되는가?**

**useEffect의 콜백함수 내에서 setState를 여러개 실행하면 중간에 리렌더링이 일어나는가?**





새로이 알아낸 것!!

setInterval을 테스트하기 위해 timer를 조절하는 방법을 찾다가 Jest의 API 문서를 보고 `runOnlyPendingTimers()` 를 알게 되었다. 문서에는 정확한 사용법보다 이럴 때 쓰는거다, 하고 예제를 던져줬는데 한번 사용한 후 interval이 진행된 이후의 상황을 테스트할 수 있는 것을 발견했다.

문제는 해당 메서드를 여러번 사용할 때였다. 2번의 인터벌을 진행하기 위해 해당 메서드를 두번 호출했지만 결과는 두번의 인터벌이 진행 된 결과가 아닌 한번의 인터벌이 진행된 결과 그대로가 출력되었다. 알고보니, 해당 메서드를 한번 호출하고 또 다시 호출하면 진행시킨 인터벌을 다시 멈추는 역할을 하고 있었다. 따라서 원하는 대로 2번의 인터벌을 진행시킨 타이밍을 잡으려면 3번, 혹은 4번을 호출해야 했다.

비동기의 타이밍 문제인 줄 알았으나 테스트메서드를 잘못사용하고 있었다. 조금 억울하지만 알아낸 나를 칭찬한다.

https://testing-library.com/docs/using-fake-timers 에서 매 테스트의 마무리에 `runOnlyPendingTimer()` 로 한번 flushing를 진행시켜 주어야 한다, 라는 말을 보고 힌트를 얻었다.





오늘 저녁 해야할 것들

=> 알고푸니!!!



https://reactjs.org/docs/test-utils.html

[Difference between enzyme, react test utils and react-testing-library](https://stackoverflow.com/questions/54152562/difference-between-enzyme-reacttestutils-and-react-testing-library/54152893#54152893)

https://www.robinwieruch.de/react-testing-library

Rather than testing a component's implementation details, React Testing Library puts the developer in the shoes of an end user of an React application.



