**debounce and throttle**

디바운스와 쓰로틀은 유저의 행동에 따라 작동하는 logic의 호출을 줄여서 어플리케이션의 효율성을 높이려 한다는 점에서 공통점을 갖는다. 그러나 디바운스와 쓰로틀은 사용되는 상황에 있어 조금 다르다.

먼저 디바운스는 유저가 인터랙션의 시도를 끝마치기 전까지는 아무 일도 하지 않고, 유저가 인터랙션을 끝마쳤다는 신호를 받았을 때에만 인풋을 로직에 전달하고 그에 해당하는 결과물을 내놓는다.

쓰로틀이 사용되는 케이스는, 유저의 인터랙션 중간중간에 인풋에 해당하는 값을 보여주되, 그 인터벌을 원하는 대로 조정하는 것을 의미한다. 인풋에 따라 실시간으로 로직을 실행하고 결과물을 반환하여 유저에게 결과물을 보여준다는 것은 같지만 로직이 실행되는 텀을 원하는 대로 조정하여 어플리케이션의 효율성을 높일 수 있는 것이다.



```javascript
const debounce = (func, wait) => {
  let timeout;

  return function executedFunction(...args) {
    const later = () => {
      //clearTimeout(timeout);
      timeout = null;
      func(...args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
};
```

코드를 확인해보면,  `func`와 `wait`를 받아 새로운 디바운스 함수를 만들어낸다. 예를 들어 유저의 인터랙션이 `5초` 간 없을 때에만 인터랙션이 끝났다고 판단하고 함수 `a`를 실행하기 위해 디바운스 함수를 호출하고, 새로운 함수 `debounceA` 만들었다고 하자.

그러면 새롭게 만들어진 `debounceA` 는 어떻게 작동하는가?

만일 사용자가 `debounceA` 를 호출하는 인터랙션을 1초에 한번씩 반복하고 있다고 가장하자.

가장 처음으로 `debounceA` 가 실행될 때, setTimeout이 호출되며 콜백으로 넘겨지는 later 함수가 5초 이후 실행되도록 계획된다. 해당 타임아웃을 멈출 수 있는 객체가 자유변수 timeout으로 할당되고, 함수실행은 끝난다.

이후 5초가 지나지 않은 시점에서 유저의 인터랙션이 일어나 또 다시 debounceA가 호출 될 때, 가장 처음으로 실행되는 것은 clearTimeout(timeout); 이다. 이를 통해 이전에 실행되도록 기다리고 있던 콜백함수의 실행을 멈출 수 있게 된다. 이후 이번 호출에 의해 새롭게 5초의 timeout을 설정하여 setTimeout을 실행한다.

이렇게

5초 이후 `later` 실행 - (5초가 지나기 전) 5초 이후 `later` 실행 - ...

무한 반복되다가, 정말 5초가 지난다면 그때 마지막으로 한번의 `later` 콜백이 실행될 수 있게 된다.

