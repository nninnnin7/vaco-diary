**Functional Programming**

함수형 프로그래밍은 퓨어펑션도 퓨어펑션이지만

...

'데이터의 변경을 예측하기 힘들다' - 어디에서 어떻게 변경하는지 알기가 힘들다.

라는 문제를 해결하기 위해

Functions has been the great solution to the problem. Functional programming **can reduce the potential impact of code** I change **by structuring our code into individual pieces where almost every single line is self-contained**.



- Pure functions - 작고 유용한 함수들
- Function composition - 그들을 조합해서 크고, 부작용 없고(주변에 해를 끼치지 않는), 유용한 함수를 만들어 사용하자
- Avoid shared state - 전역 변수의 사용을 피해 예상치 못하는 중복된 데이터 변경을 줄인다
- Avoid mutating state - 비슷한..?

- Avoid side effects - 이 또한



Reduce - reduces from 2 things to 1 thing 'repeatedly'.

```javascript

const add2 = (a) => a + 2;
const multiply3 = (a) => a * 3;
const divide5 = (a) => a / 5;

const reducer = function (input, fn) {
  return fn(input);
}

[add2, multiply3, divide5].reduce(reducer, 3);

```

상단의 예제는 함수를 인자로 받고, reducer 함수(HOF) 에 전달해서 reduce를 실행한다.

