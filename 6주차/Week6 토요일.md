2020. 9. 26

벌써 9월 26일 이라니..

오늘 할 일은!

- testing library 공부하고 프로젝트에 적용
- leetcode 풀이
- :)

**항상 잊지 말아야 할 것, 하나라도 제대로.**

조급한 마음에 이것저것 하지 말자. 하나 하나 제대로 하다 보면 그게 더 쉽고 빠른 길이라는 것을 되새기자.





오늘의 블로깅 주제는 이것..

**CRA에서의 리액트 컴포넌트 테스팅** ([참고](https://create-react-app.dev/docs/running-tests/))

​	- Jest와 Testing Library를 함께 사용하기

CRA는 약간 변형된(revamped) Jest를 사용해서 테스트를 한다. 제스트는 nodejs기반으로 작동하지만 [jsdom](https://github.com/jsdom/jsdom) 이라는 라이브러리를 활용해서 브라우저 환경을 흉내내어 dom을 브라우저 환경에서와 같이 테스팅할 수 있지만, 그건 엇비슷한 흉내에 가깝고, 제스트는 그저 DOM을 테스트하는 것이 아니라 컴포넌트와 로직을 유닛테스트 하기 위해 설계되었다.

브라우저 end-to-end 테스트를 위해서는 추가적인 라이브러리 등의 툴을 사용해야 한다 (CRA에 기본적으로 세팅되어 있지 않다).



shallow vs mount

하위 컴포넌트에 신경쓰고 싶지 않다면 shallow, 아니라면 mount.



assertion과 기대되는 (expected) 값을 제시하고 실제로 테스팅 된 값과 비교할 수 있도록 만들어 주는 것이 테스트 러너(Jest)의 역할이고, 그 이외에 구체적인 테스트 상황을 만들어 주도록 도와주는 것이 바로 테스트 유틸리티 (Enzyme 또는 React-Testing-Library) 의 역할이다.



테스트 환경 초기화할 때..

`setupFiles`와 `setupFilesAfterEnv` 는 뭐가 다른가?

https://stackoverflow.com/questions/58080435/when-should-i-use-setupfiles-rather-than-setupfilesafterenv

https://stackoverflow.com/questions/47587689/whats-the-difference-between-setupfiles-and-setuptestframeworkscriptfile

test framework가 설치되기 이전과 이후 라는 말이 무슨 의미인지 잘 모르겠다.