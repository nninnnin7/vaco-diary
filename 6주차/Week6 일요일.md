HOC? 

Higher order components.



abstracted higher order components



```javascript
import { compose } from 'recompose';

const conditionFn = (props) => !props.todos;

const withConditionalRenderings = compose(
  withLoadingIndicator,
  withCondition(conditionFn),
  withTodosEmpty
);

const TodoListWithConditionalRendering = withConditionalRederings(TodoList);

```

```javascript
const withCondition = (conditionalRenderingFn) => (Component) => (props) => {
	conditionalRenderingFn(props)
  	? null
  	: <Component {...props} />
}
```

The component returns nothing or the input component. Such a type, nothing or value, is  called 'Maybe' (or Option) in functional programming. After knowing this, you could call the higher-order component `withMaybe`.

Even though the HOC is not an explicit type, it would use the naming convention of FP to make it simple to understand.

```javascript
const withMaybe = (conditionalRenderingFn) => (Component) => (props) => {
  conditionalRenderingFn(props)
    ? null
  	: <Component {...props} />
}
```

what about the other two HOCs? They are not abstracted yet. They are different from the `withMaybe` higher-order component, because they return *either* the input component or another lement. (나머지 두개의 HOCs는 둘 중 하나가 아니라 두개 다 리턴할 수도 있다.)



`withEither` 는 매개변수로 conditional Fn 이외에 다른 하나의 Component를 받고, 조건이 맞지 않을 때에는 null이 아닌 해당 컴포넌트를 렌더링한다.



```javascript
// 이게 가만히 있으면 눈에 들어오겠냐고.

import { compose } from 'recompose';

const EmptyMessage = () => {
  return (
  	<div>
    	<p>You have no Todos.</p>
    </div>
  )
}

const LoadingIndicator = () => {
  return (
  	<div>
    	<p>Loading todos...</p>
    </div>
  )
}

const isLoadingConditionFn = (props) => props.isLoadingTodos;
const nullConditionFn = (props) => !props.todos;
const isEmptyConditionFn = (props) => !props.todos.length;

// HOCs 조합하기 (compose)

const withConditionalRenderings = compose(
	withEither(isLoadingConditionFn, LoadingIndicator),
  withMaybe(nullConditionFn),
  withEither(isEmptyConditionFn, EmptyMessage)
);




```

