Redux-Thunk

[참고자료]([https://www.digitalocean.com/community/tutorials/redux-redux-thunk#:~:text=Redux%20Thunk%20is%20a%20middleware,the%20asynchronous%20operations%20have%20completed.](https://www.digitalocean.com/community/tutorials/redux-redux-thunk#:~:text=Redux Thunk is a middleware,the asynchronous operations have completed.))

그러니까 thunk는,

action creator가 액션을 바로 리턴하는게 아니라 함수로 감싸진 것을 리턴하고, 해당 함수 안에서 비동기 요청 등이 완료되었을 때 액션을 리턴하는 함수를 dispatch 할 수 있도록 wrapping하는 것을 의미한다.

