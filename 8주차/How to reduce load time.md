주제를 조금 한정시켜 보기로 생각해서 나중에 내가 맡게 될 프로젝트는 리액트 앱일 것 이기 때문에..아래와 같은 글을 읽어보게 되었다.



[**Optimizing loading time for big React apps**](https://medium.com/front-end-weekly/optimizing-loading-time-for-big-react-apps-cf13bbf63c57)



Optimizing the load time is a really important, because higher loading times are associated with high bounce rates and poor conversion rates. A big bottleneck in the loading time are usually our **bundle size** and the **loading of external resources**.



웹팩을 잘 알아야 하는 시간이군요.



## Load just what you need

A big problem with named imports in ES6 is that you are importing the whole library and then destructuring it to access the module you need.



For small libs, it's ok, you will not notice any change, but big dependencies, like lodash or ramda, ... encourage you to just import what you need.



```javascript
import { find } from 'lodash';

// 가 아니라

import find from 'lodash/find';

```

This is usually called `cherry picking`. 



**Uglify!!**

![image-20201009112134388](/Users/mini_06/Library/Application Support/typora-user-images/image-20201009112134388.png)

1주에 1300만 다운로드..

UglifyJS is a JavaScript parser, minifier, compressor and beautifier toolkit.



Minification tool 로 불리는 듯..

Minification (also minimization) is the process of **removing all unnecessary characters** from the source  code of interpreted programming languages or markup language without changing their functionality.

These 'unnecessary characters' usually include:

- Whitespace characters
- Newline characters
- Comments
- Block delimiters (구분자, 구획자)



소포의 크기를 최소화시켜야 택배를 나르는 분들이 상하차, 배달 등을 할 때 덜 힘들듯..





예제보다가..

**for - in**

Although arrays are often more practical for string data, in situations where a key-value pairs is preferred for working with data, there may be instances where you wnat to check if any of those keys hold a particular value.



UglifyJS는 바벨의 기능 (최신 신택스를 이전의 범용적인 신택스로 변형) 을 지원하지 않기 때문에, 어차피 바벨을 써야한다면 바벨과 함께 이 기능을 이어주자 - 라는 느낌으로 babel-minify 가 탄생하게 되었다고.

바벨로 transfile -> uglifyJS

을

바벨로 transfile + babel-minify 플러그인 적용

의 한 스텝으로 줄일 수 있다.

그러나 experimental하며 production에는 아직 사용하지 말라고 한다고..



Webpack은 Terser를 기본 minifier로 사용한다!