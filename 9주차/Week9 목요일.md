mongoose.connect를 어디에서 하든 상관없다.



mongoose.connection은 `연결된 상태` 자체를 의미하며, 연결된 상태에서 무언가를 하기 위해 참조되는 객체이다. (아마도)



[mongoose Model](https://mongoosejs.com/docs/models.html)

Every model has an associated connection (with Collections in DB). When you use `mongoose.model()`, your model will use the default mongoose connection.



mongoose.connect가 아니라

mongoose.createConnection으로 custom connection을 생성했을 경우, 해당 인스턴스의 connection.model() 메서드를 사용해야 한다.(몽고디비의 컬렉션과 몽구스 스키마를 이어주어 모델을 만들기 위해)



결국 모델을 사용해서 쿼리, update, delete 등을 할 때 마다 connect 된 connection (;;) 을 사용한다는 것!



커넥션 자체에 볼일이 있을때는 mongoose.connection 객체를 이용한다 (커스텀일때는 커스텀 커넥션 객체를 이용)



과정..

- 0번째 유저의 아이디를 가져와서
- 토큰을 받는다
- /articles/new 로 post 요청을 보낸다
- 토큰은 아까 받아온걸로
- newArticle을 담아서..
- 그럼 201이 와야한다 (성공, 201 created)



- 받아온 녀석은 존재하고, 데이터는

  ```javascript
  {
    result: 'ok',
    article: article
  }
  ```

  

[SchemaTypes?](https://mongoosejs.com/docs/schematypes.html)

A SchemaType says what type a given path should have, whether it has any getters/setters, and what values are valid for that path.





jwt `iat` - `issued at`.



Decode 결과가 undefined라면..





이번엔 update 시나리오

- 1번 유저 id와 1번 아티클 id를 가져온다
- 유저 id로 sign된 토큰을 발급받는다 (/users/userId/token 으로 get 요청)
- /articles/articleId 로 put 요청을 보낸다
- 아까 받아온 토큰도 함께 보낸다.
- 토큰 정보를 middleware에서 체크하고, valid하지 않다면 업데이트를 시행하지 않고 401 unauthorized 에러를 리턴할 것
- 우리 토큰은 유효하므로..



[findOneAndUpdate 와 updateOne 의 차이 (Stack Overflow)](https://stackoverflow.com/questions/31808786/mongoose-difference-of-findoneandupdate-and-update/31809167)

