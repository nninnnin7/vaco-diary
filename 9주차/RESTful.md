**REST - Representational state transfer**

Roy Fielding



>  By using a stateless protocol and standard operations, Restful systems aim for fast performance, reliability, and the ability to grow by reusing components that can be managed and updated without affecting the system as a whole, even while it is running.



이번주 블로그에 정리

API를 'RESTful' 하게 작성함으로써 서버는 클라이언트에게 '야, 너는 이거 구조는 알 필요 없고 그냥 니가 갖고가고싶은 것만 말해' 라고 말할 수 있다.



How does RESTful can be implemented to HTTP in practice?

HTTP is oriented around verbs and resources.



The client and the server have both been programmed to understand this format, but they don't know anything about each other.

As Roy Fielding points out..

> A REST API should spend almost all of its descriptive effort in defining the media types used for representing resources and driving application state, or in defining extended relation names and/or hypertext-enabled mark-up for existing standard media types.





**Stateless**

요청을 보낼 때, '요청자' 의 state에 관여하지 않는다..는 말인 즉슨.

요청자의 상태(state)에 관계 없이 요청을 보낼 수 있다, 라는 의미인 듯 하다.

즉 요청자의 상태가 어떻듯 같은 request를 보낼 수 있다 (request가 요청자의 state에 영향을 받지 않는다 - )



그럼, 만약 stateful 하다면?

authentication을 예로 들면, 클라이언트 A가 특정 세션 정보를 가지고 있지 않다면 서버 B에 요청을 보내 원하는 데이터를 가져오지 못할 수 있다. 이러한 요청은 stateful하다고 볼 수 있다.

stateful 한 것이 꼭 나쁜 것은 아니다.

**Cacheable**

**Uniform interface**

보편적인 인터페이스를 가지고 범용성 높은 API를 만들 수 있다.

**Layered System**

**Self-Descriptive**



Method와 Endpoint

Method는 동사 (GET, POST, PUT, DELETE)

Endpoint는 명사 (articles, videos, ...)



`/users/getUsers/fromChannelId/{channelId}`

`/users?type=subscriber&channelId={channelId}`

무엇이 더 보기 좋은지..



Authentication 이 '인증 요청' 이고

Authorization이 '인증'



하나의 비밀키를 두고 인증을 하는 방식일 때, 클라이언트가 하나의 비밀키를 털리면 끝장

해당 비밀키로 풀 수 있는 토큰을 서버가 보내주고, 클라이언트가 서명 후 validation 된 토큰을 한 세션에서 계속 활용할 경우? 비밀키를 노출시키는 위험을 줄일 수 있지 않을까..



[조대협님 블로그](https://bcho.tistory.com/955?category=252770) 에서는 다음과 같이 설명..

>  사용자 PW는 주기적으로 바뀔 수 있고, 매번 네트워크를 통해 사용자 ID-PW를 보내는 것은 보안적으로 사용자 계정 정보를 탈취당할 가능성이 높기 때문에 API Token을 별도로 발급해서 사용하는 것이다.

그렇다. 그러니까 클라이언트가 서버로 ID-PW를 보내는게 아니라, 서버에서 ID-PW를 검증하는 기능(sign - verify)을 가진 데이터를 토큰화 시켜 보내주면 클라이언트는 클라이언트 측에서 가지고 있는 정보로 이 토큰을 verification하여 비밀 정보를 사용할 수 있게 되는 것. 뭐랄까..보안택 같은거랄까..사물함까지 가서 비밀번호를 입력하고 짐을 가져오는게 아니라 사물함 측에서(?) 자물쇠를 채워서 중요한 정보만 담은 상자..?를 보내준 것이랄까..



Authorization 헤더는 401 Unauthorized 응답이 www-Authenticate 헤더와 함께 도착했을 때 request에서 인증을 위한 데이터(주로 아이디와 비밀번호를 base64로 인코딩 한 정보)와 함께 재요청을 보낼 때 포함시키는 메타데이터.



OAuth?

삼촌 양말



Digest access Authentication?

HTTP Basic Auth의 보안이 취약하다는 단점을 보완하여 나온 인증 프로토콜이 Digest access Authentication. 



`Access-Control-Allow-Origin: *`



여기서 CORS가 왜나오지?

bcryptjs  - hashing pw

cors - will add a header to the url and allow an external url to correctly read the information on the url called.