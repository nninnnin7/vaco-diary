어떤 One-to-N 스키마 디자인을 선택할 것인가?



고려사항

- 'N' 사이드의 녀석들이 stand-alone일 필요가 있는가? 예를 들어, '유저의 사진' 이 아니라 '-인 조건의 사진' 으로 검색될 필요성이 있는가? (독립적인 쿼리의 필요성)
- Cardinality of the relationship
  - One-to-few
  - One-to-many
  - One-to-squillions



이 두가지 사항을 고려하여 선택할 수 있는 3가지의 