패스포트로 로그인 구현







http가 stateless하기 때문에, in order to associate a request to any other request, you need a way to store user data between HTTP request.



Cookies or URL parameters are both suitable ways to transport data between 2 or more request.



쿠키나 url parameter가 적합하다.

**to transport data between 2 or more request.**



하지만 그것들은 not good in case you don't want that data to be readable/editable on client side.



아예 데이터 자체를 실어보내는것은 안된다.



솔루션 - 'id' 를 사용한다.



The solution is to store that data server side, give it an 'id', and let the client only know that id.



데이터 자체는 서버에 저장하고, 'id' 를 부여 한 다음 클라이언트는 아이디만 알려준다. (쿠키에 담아 전송한다)



Of course there are other aspects to consider, like you don't want pepople to hijack other's sessions, you want sessions to not last forever but to expire, and so on.



세션의 만료일을 설정하면, 만약 세션 아이디를 가로채인다 하더라도 새로운 세션이 만들어져 있을 것이기 때문에 관계 없게 된다.



the user id is stored in the session data, server-side, after successful identification.

1. 성공적인 authentication
2. session에 유저 아이디 저장
3. 



Then for every HTTP request you get from the client, the session id will point you to the correct session data that contains the authenticated user id - that way your code will know what user it is talking to.



