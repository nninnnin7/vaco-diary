https://stackoverflow.com/questions/23169941/what-does-bin-www-do-in-express-4-x

The `bin/` directory serves as a location where you can define your various **startup scripts.**

The `www` is an example to start the express app as a web server.



Ultimately, you could have different scripts like `test`, `stop`, or `restart`, etc. Having this structure allows you to have different startup configurations, without cramming everything into `app.js`.

