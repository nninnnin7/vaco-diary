몽구스에도 훅이 있고 미들웨어가 있다.



몽구스의 미들웨어

- document middleware
  - validate (save 이전에 실행되고, if a validation rule is violated, save is aborted and error is returned to you callback function)
  - save
  - remove
  - updateOne
  - deleteOne
  - init
- model middleware
- aggregate middleware
- query middleware