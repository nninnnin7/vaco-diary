자바스크립트의 특별한 자료형 Map과 Set에 대해 알아보자.



Map is a collection of keyed data items, just like an Object. 객체랑 비슷한 구실을 한다. 

But the main difference is that `Map` allows keys of any type.

**Object와 Map이 결정적으로 다른 점은, Object는 키로 string만 사용이 가능하지만 Map은 어떤 타입의 자료형이든 사용이 가능하다는 것이다.**



Set 은 번역하자면 '집합' 이다. 

"Set of values", where each value may occur only once.

Map과 마찬가지로 **Set 내에서 모든 값들은 중복되지 않고 only one으로 존재한다. 단! Set 은 key를 사용하지 않고 value만 저장한다.**





쉽게 이해하자면, Map은 key로 문자열 뿐 아니라 어떤 타입의 데이터든 사용이 가능하고 값끼리의 중복을 허용하지 않는 업그레이드 된(?) 객체 라고 볼 수 있겠고, Set은 값끼리 중복을 허용하지 않는 업그레이드 된..; 배열이라고 볼 수 있을 것 같다.



예를 들어, Set은 이런 상황에서 사용할 수 있다. 방문자를 기록하고 싶을 때 (동명이인은 생각하지 않는다) 이 방문한 사람의 횟수와 상관 없이 그저 이름만을 기록하고 싶을 때, 사용할 수 있겠다. `set.add(value)` 로 set에 중복된 값을 추가하려고 시도해도 중복된 값은 추가적으로 집합 내에 생성되지 않는다. 



The alternative to Set could be an array of users, and the code to check for duplicates on every insertion using arr.find. 

말인 즉슨, 셋을 쓰지 않고 기존의 배열을 사용할수도 있으나 셋과 같이 배열 내의 값들이 중복되지 않도록 관리하려면 값을 추가할 때 마다 arr.find() 로 검색을 시행해야 한다는 말이다. O(n)의 시행을 반복해야 하니, 매우 비효율적일 것이다. 그럼 Set은 내부적으로 어떻게 중복을 파악할까?

Set is much better optimized internally for uniqueness checks.

내부적으로 더 최적화 되어있다고 한다.





**Iteration over Set**

배열과 같이, `for..of` 또는 `forEach` 로 순회가 가능하다.

단 배열과 달리 `forEach` 를 사용할 때, 콜백의 인자들이 `(value, index, array)` 가 아니라 `(value, valueAgain, set)` 으로, 두번째 인자에 인덱스 대신 값이 전달된다.







### **Map은 Object와 다르게, Insertion order를 기억한다**

여기서 궁금증, object는 정말 들어온 키-값 들의 순서를 무작위로 그냥 가지고 있는건가?

[참고 stack overflow - Does Javascript Guarantee Object Property Order?](https://stackoverflow.com/questions/5525795/does-javascript-guarantee-object-property-order)



es5에서는 전혀 순서가 보장되지 않았지만, es6 (es2015) 부터는 특정한 상황에 순서를 갖게 되었다.

1. integer-like keys in ascending order

2. normal keys in **insertion order**

3. Symbols in **insertion order**

   그러니까 - 1,2,3,4 와 같은 integer-like key들은 insertion order의 영향을 받지 않고, 다른 키들은 insertion order의 영향을 받는다.



**이하 메소드 / 룹은 순서를 '전혀 보장하지 않는다' !** (근데 써보면 보장하는 것 처럼 보이긴 한다.)

- Object.keys
- for..in
- JSON.parse
- JSON.stringify



결론 ! Even in ES2015 you shouldn't rely on the property order of normal objects in Javascript. It is prone to errors. **Use `Map` instead.**





