https://medium.com/@ryanchenkie_40935/react-authentication-how-to-store-jwt-in-a-cookie-346519310e81

**Where should JSON Web Tokens be stored?**

JWT는 어디에 저장되어야 하는가?

1. 로컬스토리지
2. 쿠키
3. 리액트 스테이트

 local storage에 저장하지 않는다면 JWT가 저장될 수 있는곳은 어디가 있을까?



1. 브라우저 메모리 (React state)
2. HttpOnly cookie

첫번째 옵션이 더 안전하다, 왜냐하면 쿠키에 들어간 JWT는 **남아있는 토큰** 의 위험성을 확실히 없애주지 않는다..(토큰이 스틸당할 경우)

HttpOnly Cookie라 하더라도 어떤 공격자들은 (쿠키에서) 토큰을 훔쳐서 유저인 척 어떤 요청을 보내버릴 수 있다.



그러나 첫번째 옵션 (브라우저 메모리, React state)가 언제나 실용적이지는 않다. 왜냐하면 페이지가 새로고침되거나 닫혀질 때 JWT가 손실될 수 있기 때문이다. 그렇다면 유저는 매번 새로고침할 때마다 로그인을 다시 해야한다..



