인증 (Authentication) 의 구현 방식

- 세션과 쿠키
- 토큰



**세션과 쿠키**

사용자가 임의로 삭제하는 등 쿠키가 사라졌을 때에도 어플리케이션이 정상적으로 작동해야한다.

쿠키는 용량에 제한이 있다(normally **4096 Bytes**). - 사용자 데이터를 전부 저장하는 것이 아니라, 세션Id 등 사용자를 식별할 수 있는 데이터만을 저장하고 세션 데이터는 서버측에 저장한다.

세션 Id 가 hijack되는 등 사용자의 identification data의 유출 위험을 줄이기 위해 보통 쿠키와 세션은 expire span을 갖는다.



session을 DB에 저장한다 or 안한다 는 서버가 끄고 켜질때 세션이 유지되는가 아닌가.



서버가 쿠키를 지우지는 못하겠지?



세션-쿠키 방식의 장점

- 서버에서 사용자 인증을 관리하기 때문에 XSS 공격으로부터 안전.

세션-쿠키 방식의 단점

- CSRF에 취약
- 브라우저가 아닌 모바일 대응이 힘들다





토큰 방식을 사용할 경우 토큰을 도난당하면 큰 위험(모든 정보가 사용 가능)이 있기 때문에, expire span을 아주 짧게 설정하고 자주 refresh해준다.